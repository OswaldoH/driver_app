import 'package:flutter/material.dart';

import 'package:driver_app_v2/src/splash/presentation/screens/splash_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Driver App',
      debugShowCheckedModeBanner: false,
      home: SplashScreen(),
    );
  }
}
